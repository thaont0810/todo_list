export default class Task {
  constructor(id = '', title = '', desc = '', due_date = '', priority = 2) {
    this.id = id
    this.title = title
    this.description = desc
    this.due_date = due_date
    this.priority = priority
  }
}
