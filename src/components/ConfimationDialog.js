import React from 'react'
import { Pane, Dialog, Button, Strong } from 'evergreen-ui'

export default function ConfirmationDialog(props) {
  const { isShown, deletingTask, confirmRemove, cancelRemoveTask, onCloseComplete } = props

  const onYes = () => {
    confirmRemove()
  }

  const onNo = () => {
    cancelRemoveTask()
  }
  return (
    <>
      <Pane>
        <Dialog isShown={isShown} title='Remove task' hasFooter={false} onCloseComplete={onCloseComplete}>
          <Pane>
            Are you sure to remove{' '}
            {deletingTask ? (
              <span>
                <Strong>{deletingTask.title}</Strong> task
              </span>
            ) : (
              <span>those tasks</span>
            )}{' '}
            from Todo list
          </Pane>

          <Pane className='c-action c-form__action' display='flex'>
            <Button type='button' className='c-button c-button--blue' appearance='default' onClick={() => onNo()}>
              Cancel
            </Button>
            <Button type='button' className='c-button' appearance='primary' intent='danger' onClick={() => onYes()}>
              Remove
            </Button>
          </Pane>
        </Dialog>
      </Pane>
    </>
  )
}
