import React from 'react'
import { Pane } from 'evergreen-ui'

export default function NotFound(props) {
  return <Pane className='c-component--empty'>No Tasks</Pane>
}
