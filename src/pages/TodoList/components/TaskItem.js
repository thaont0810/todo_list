import React, { useState, useEffect } from 'react'
import { Pane, Button, Checkbox, TextInputField, FormField, TextareaField, SelectField } from 'evergreen-ui'
import DatePicker from 'react-datepicker'
import Task from '../../../models/Task'
import { PRIORITIES } from '../../../utils/Constant'
import 'react-datepicker/dist/react-datepicker.css'

export default function TaskItem(props) {
  const {
    task,
    onRemove,
    toggleConfirmationDialog,
    onUpdateTask,
    onSelectTask,
  } = props
  const [editingTask, setEditingTask] = useState(new Task())
  const [isDetailShown, setDetailShown] = useState(false)
  const [isChecked, setChecked] = useState(false)

  const handleRemove = (item) => {
    toggleConfirmationDialog()
    onRemove(item)
  }

  const onFieldChange = (field, e, date = null) => {
    const newEditingTask = { ...editingTask }
    if (field === 'due_date' && e === null && date) {
      newEditingTask[field] = date
    } else {
      newEditingTask[field] = e.target.value
    }
    setEditingTask(newEditingTask)
  }

  const handleShowDetail = () => {
    setDetailShown(!isDetailShown)
  }

  const handleSubmit = () => {
    onUpdateTask(editingTask)
  }

  const toggleCheckbox = (e, item) => {
    const checked = e.target.checked
    if (checked) {
      setChecked(true)
      onSelectTask(item.id)
    } else {
      setChecked(false)
      onSelectTask(item.id)
    }
  }

  useEffect(() => {
    setEditingTask(task)
  }, [task])

  return (
    <>
      <Pane is='li' className='c-item'>
        <Pane className='c-item__top' display='flex'>
          <Pane className='c-item__name text-bold' display='flex'>
            <Checkbox marginRight={10} checked={isChecked} onChange={(e) => toggleCheckbox(e, task)} />
            {task.title}
          </Pane>
          <Pane className='c-action c-item__action'>
            <Button
              type='button'
              className='c-button c-button--blue'
              appearance='default'
              onClick={() => handleShowDetail()}
            >
              Detail
            </Button>
            <Button
              type='button'
              className='c-button'
              appearance='primary'
              intent='danger'
              onClick={() => handleRemove(task)}
            >
              Remove
            </Button>
          </Pane>
        </Pane>
        {isDetailShown && (
          <Pane className='c-item__content'>
            <form onSubmit={handleSubmit} className='c-form'>
              <Pane>
                <TextInputField
                  name='title'
                  label='Title'
                  required
                  placeholder='Enter new task...'
                  value={editingTask.title}
                  onChange={(e) => onFieldChange('title', e)}
                />
              </Pane>
              <Pane>
                <TextareaField
                  name='description'
                  label='Description'
                  value={editingTask.description}
                  onChange={(e) => onFieldChange('description', e)}
                />
              </Pane>
              <Pane display='flex' className='c-row--50'>
                <Pane className='col'>
                  <FormField label='Due Date'>
                    <DatePicker
                      name='due_date'
                      selected={new Date(editingTask.due_date)}
                      minDate={new Date()}
                      dateFormat='dd MMMM yyyy'
                      onChange={(date) => onFieldChange('due_date', null, date)}
                    />
                  </FormField>
                </Pane>
                <Pane className='col'>
                  <SelectField
                    name='priority'
                    label='Priority'
                    children={
                      PRIORITIES &&
                      PRIORITIES.map((priority, index) => (
                        <option value={priority.value} key={index} name='priority'>
                          {priority.label}
                        </option>
                      ))
                    }
                    defaultValue={editingTask.priority}
                    onChange={(e) => onFieldChange('priority', e)}
                  />
                </Pane>
              </Pane>
              <Pane>
                <Button type='submit' className='c-button c-button--save' appearance='primary' intent='success'>
                  Update
                </Button>
              </Pane>
            </form>
          </Pane>
        )}
      </Pane>
    </>
  )
}
