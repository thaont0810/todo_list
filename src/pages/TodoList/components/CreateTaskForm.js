import React, { useState } from 'react'
import { Pane, TextInputField, TextareaField, SelectField, Button, FormField } from 'evergreen-ui'
import DatePicker from 'react-datepicker'
import Task from '../../../models/Task'
import { PRIORITIES } from '../../../utils/Constant'
import 'react-datepicker/dist/react-datepicker.css'

const defaultPriority = 2

export default function CreateTaskForm(props) {
  const { onFormSubmit } = props
  const [task, setTask] = useState(new Task())
  const [dueDate, setDueDate] = useState(new Date())

  const onFieldChange = (field, e, date = null) => {
    const newTask = { ...task }
    if (field === 'due_date' && e === null && date) {
      setDueDate(date)
      newTask[field] = date
    } else {
      newTask['due_date'] = dueDate || new Date()
      newTask[field] = e.target.value
    }
    setTask(newTask)
  }

  const handleSubmit = () => {
    onFormSubmit(task)
    setTask(new Task())
  }

  return (
    <>
      <form onSubmit={handleSubmit} className='c-form'>
        <Pane>
          <TextInputField
            id='task-title'
            name='title'
            label='Title'
            required
            placeholder='Enter new task...'
            value={task.title}
            onChange={(e) => onFieldChange('title', e)}
          />
        </Pane>
        <Pane>
          <TextareaField
            id='task-desc'
            name='description'
            label='Description'
            value={task.description}
            onChange={(e) => onFieldChange('description', e)}
          />
        </Pane>
        <Pane display='flex' className='c-row--50'>
          <Pane className='col'>
            <FormField label='Due Date'>
              <DatePicker
                name='due_date'
                selected={dueDate}
                minDate={new Date()}
                dateFormat='dd MMMM yyyy'
                onChange={(date) => onFieldChange('due_date', null, date)}
              />
            </FormField>
          </Pane>
          <Pane className='col'>
            <SelectField
              id='task-priority'
              name='priority'
              label='Priority'
              children={
                PRIORITIES &&
                PRIORITIES.map((priority, index) => (
                  <option value={priority.value} key={index} name='priority'>
                    {priority.label}
                  </option>
                ))
              }
              defaultValue={defaultPriority}
              onChange={(e) => onFieldChange('priority', e)}
            />
          </Pane>
        </Pane>
        <Pane>
          <Button type='submit' className='c-button c-button--save' appearance='primary' intent='success'>
            Add
          </Button>
        </Pane>
      </form>
    </>
  )
}
