import React, { useState } from 'react'
import { Pane, TextInputField } from 'evergreen-ui'

export default function SearchTask(props) {
  const { onSearchTask } = props
  const [query, setQuery] = useState('')

  const onFieldChange = (e) => {
    let newSearchText = { ...query }
    newSearchText = e.target.value
    setQuery(newSearchText)
    onSearchTask(newSearchText)
  }

  return (
    <>
      <form className='c-form c-form__search'>
        <Pane>
          <TextInputField
            id='search-title'
            name='title'
            label=''
            placeholder='Search a task'
            value={query}
            onChange={(e) => onFieldChange(e)}
          />
        </Pane>
      </form>
    </>
  )
}
