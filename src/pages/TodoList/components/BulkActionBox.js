import React from 'react'
import { Pane, Button } from 'evergreen-ui'

export default function BulkActionBox(props) {
  const { onOpenBulkModal } = props

  const handleOpenBulkModal = () => {
    onOpenBulkModal()
  }

  return (
    <>
      <Pane className='c-action--fixed'>
        <Pane className='container' display='flex' justifyContent='space-between'>
          <span>Bulk Action:</span>
          <Button
            type='button'
            className='c-button'
            appearance='primary'
            intent='danger'
            onClick={() => handleOpenBulkModal()}
          >
            Remove
          </Button>
        </Pane>
      </Pane>
    </>
  )
}
