import React, { useState } from 'react'
import TaskItem from './TaskItem'
import ConfirmationDialog from '../../../components/ConfimationDialog'
import Task from '../../../models/Task'

export default function TaskList(props) {
  const { listTasks, handleDeleteTask, handleUpdateTask, handleSelectMultiple } = props
  const [deletingTask, setDeletingTask] = useState(new Task())
  const [isDialogShown, setDialogShown] = useState(false)
  const [selectedCheckboxes, setSelectedCheckboxes] = useState([])

  const toggleConfirmationDialog = () => {
    setDialogShown(!isDialogShown)
  }

  const onRemovingTask = (item) => {
    const deletingTask = { ...item }
    setDeletingTask(deletingTask)
    toggleConfirmationDialog()
  }

  const confirmRemove = () => {
    handleDeleteTask(deletingTask.id)
    toggleConfirmationDialog()
  }

  const cancelRemoveTask = () => {
    setDialogShown(false)
  }

  const onSelectTask = (id) => {
    let arr = [...selectedCheckboxes]
    if (arr.indexOf(id) > -1) {
      const newArr = arr.filter((item) => {
        return item !== id
      })
      setSelectedCheckboxes(newArr)
      handleSelectMultiple(newArr)
    } else {
      arr.push(id)
      setSelectedCheckboxes(arr)
      handleSelectMultiple(arr)
    }
  }

  return (
    <>
      <ul className='c-list'>
        {listTasks &&
          listTasks.map((task) => (
            <TaskItem
              task={task}
              key={task.id}
              onRemove={onRemovingTask}
              toggleConfirmationDialog={() => toggleConfirmationDialog()}
              onUpdateTask={handleUpdateTask}
              onSelectTask={onSelectTask}
            />
          ))}
      </ul>

      <ConfirmationDialog
        isShown={isDialogShown}
        confirmRemove={confirmRemove}
        cancelRemoveTask={cancelRemoveTask}
        deletingTask={deletingTask}
        onCloseComplete={() => {
          setDialogShown(false)
        }}
      />
    </>
  )
}
