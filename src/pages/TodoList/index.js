import React, { useState, useEffect } from 'react'
import { Pane, Heading, Button, Dialog } from 'evergreen-ui'
import { ulid } from 'ulid'
import dayjs from 'dayjs'
import NotFound from '../../components/NotFound'
import ConfirmationDialog from '../../components/ConfimationDialog'
import CreateTaskForm from './components/CreateTaskForm'
import TaskList from './components/TaskList'
import SearchTask from './components/SearchTask'
import BulkActionBox from './components/BulkActionBox'

export default function TodoList() {
  const [isDialogShown, setDialogShown] = useState(false)
  const [tasks, setTasks] = useState([])
  const [filteredTasks, setFilteredTasks] = useState([])
  const [isDialogDeleteShown, setDialogDeleteShown] = useState(false)
  const [deletingTasks, setDeletingTasks] = useState([])
  const [isDeleteBulkShown, setDeleteBulkShown] = useState(false)

  const toggleShowDialog = () => {
    setDialogShown(!isDialogShown)
  }

  const onFormSubmit = (task) => {
    task.id = ulid()
    tasks.push(task)
    localStorage.setItem('tasks', JSON.stringify(tasks))
    toggleShowDialog(true)
  }

  const onSearchTask = (text) => {
    if (text) {
      const filteredTasks = tasks.filter((task) => {
        const taskTitle = task.title.trim().toLowerCase()
        const trimValue = text.trim().toLowerCase()
        return taskTitle.indexOf(trimValue) !== -1
      })
      setFilteredTasks(filteredTasks)
    } else {
      setFilteredTasks(tasks)
    }
  }

  const handleDeleteTask = (id) => {
    const newFilterTasks = tasks.filter((task) => task.id !== id)
    localStorage.setItem('tasks', JSON.stringify(newFilterTasks))
    setTasks(newFilterTasks)
    setFilteredTasks(newFilterTasks)
  }

  const handleUpdateTask = (item) => {
    const newFilterTasks = tasks.map((task) => {
      if (task.id === item.id) {
        const updatedTask = { ...item }
        return updatedTask
      }
      return task
    })
    localStorage.setItem('tasks', JSON.stringify(newFilterTasks))
    setTasks(newFilterTasks)
  }

  const handleSelectMultiple = (arr) => {
    if (arr.length > 0) {
      setDeleteBulkShown(true)
    } else {
      setDeleteBulkShown(false)
    }
    setDeletingTasks(arr)
  }

  const onOpenBulkModal = () => {
    setDialogDeleteShown(true)
  }

  const confirmRemove = () => {
    var remainTasks = tasks.filter((task) => {
      return !deletingTasks.includes(task.id)
    })
    setTasks(remainTasks)
    setFilteredTasks(remainTasks)
    localStorage.setItem('tasks', JSON.stringify(remainTasks))
    setDialogDeleteShown(false)
  }

  const cancelRemoveTask = () => {
    setDialogDeleteShown(false)
  }

  const fetchTasks = () => {
    let initialTasks
    const savedTasks = localStorage.getItem('tasks')
    if (savedTasks) {
      try {
        initialTasks = JSON.parse(savedTasks)
        const newTasks = Array.from(initialTasks).sort((a, b) => {
          return dayjs(a.due_date).format('YYYYMMDD') - dayjs(b.due_date).format('YYYYMMDD')
        })
        setTasks(newTasks)
        setFilteredTasks(newTasks)
      } catch (err) {
        console.log(err)
      }
    } else {
      initialTasks = []
      localStorage.setItem('tasks', JSON.stringify(initialTasks))
    }
    return initialTasks
  }

  useEffect(() => {
    fetchTasks()
  }, [])

  return (
    <>
      <Pane className='app'>
        <Pane className='container container-2'>
          <Heading is='h1' className='header-text text-center text-bold' size={800} marginY={30}>
            Todo List
          </Heading>
          <Pane>
            <Button
              type='button'
              className='c-button text-bold'
              appearance='primary'
              intent='success'
              onClick={() => toggleShowDialog()}
            >
              Create new task
            </Button>
            <Dialog
              isShown={isDialogShown}
              title='Create new task'
              hasFooter={false}
              onCloseComplete={() => {
                setDialogShown(false)
              }}
            >
              <CreateTaskForm onFormSubmit={onFormSubmit} />
            </Dialog>
          </Pane>
          <SearchTask onSearchTask={onSearchTask} />
          {filteredTasks.length > 0 ? (
            <TaskList
              listTasks={filteredTasks}
              handleDeleteTask={handleDeleteTask}
              handleUpdateTask={handleUpdateTask}
              handleSelectMultiple={handleSelectMultiple}
            />
          ) : (
            <NotFound />
          )}
        </Pane>

        {isDeleteBulkShown && <BulkActionBox onOpenBulkModal={onOpenBulkModal} />}
        <ConfirmationDialog
          isShown={isDialogDeleteShown}
          confirmRemove={confirmRemove}
          cancelRemoveTask={cancelRemoveTask}
          onCloseComplete={() => {
            setDialogDeleteShown(false)
          }}
        />
      </Pane>
    </>
  )
}
