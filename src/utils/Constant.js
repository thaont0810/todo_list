export const PRIORITIES = [
  {
    value: 1,
    label: 'Low',
  },
  {
    value: 2,
    label: 'Normal',
  },
  {
    value: 3,
    label: 'High',
  },
]
